package gql

import (
	"net/http"
)

// Request is a GraphQL request
type Request struct {
	Query     string                 `json:"query"`
	Variables map[string]interface{} `json:"variables"`
	Header    http.Header            `json:"-"`
}

// NewRequest returns a new Request
func NewRequest(query string) *Request {
	return &Request{
		Query:     query,
		Variables: make(map[string]interface{}),
		Header:    make(map[string][]string),
	}
}
