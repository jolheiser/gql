package gql

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

// Client is a client aimed at a specific GraphQL endpoint
type Client struct {
	Endpoint string
	Options  *ClientOptions
}

// ClientOptions are options that can be set for a Client
type ClientOptions struct {
	HTTP *http.Client
}

var (
	// DefaultOptions are the default set of ClientOptions for a Client
	DefaultOptions = &ClientOptions{
		HTTP: http.DefaultClient,
	}

	defaultHeader = http.Header{
		"Content-Type": []string{"application/json; charset=utf-8"},
		"Accept":       []string{"application/json; charset=utf-8"},
	}
)

// NewClient returns a new Client for making GraphQL requests
func NewClient(endpoint string, options *ClientOptions) *Client {
	if options == nil {
		options = DefaultOptions
	}
	return &Client{
		Endpoint: endpoint,
		Options:  options,
	}
}

// Run runs a given Request using the Client
func (c *Client) Run(ctx context.Context, req *Request, resp interface{}) error {
	select {
	case <-ctx.Done():
		return ctx.Err()
	default:
	}
	return c.json(ctx, req, resp)
}

func (c *Client) json(ctx context.Context, req *Request, resp interface{}) error {
	requestBody, err := json.Marshal(req)
	if err != nil {
		return fmt.Errorf("encode request body: %v", err)
	}

	r, err := http.NewRequest(http.MethodPost, c.Endpoint, bytes.NewBuffer(requestBody))
	if err != nil {
		return err
	}
	r.Header = defaultHeader
	for key, values := range req.Header {
		for _, value := range values {
			r.Header.Add(key, value)
		}
	}
	r = r.WithContext(ctx)

	res, err := c.Options.HTTP.Do(r)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	responseBody, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return fmt.Errorf("reading body: %v", err)
	}

	gr := &Response{
		Data: resp,
	}
	if err := json.Unmarshal(responseBody, &gr); err != nil {
		if res.StatusCode != http.StatusOK {
			return fmt.Errorf("non-200 status code response: %v", res.StatusCode)
		}
		return fmt.Errorf("decoding response: %v", err)
	}

	if len(gr.Errors) > 0 {
		return gr.Errors
	}
	return nil
}
