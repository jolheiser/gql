package gql

import "strings"

// Error is an individual GraphQL error
type Error struct {
	Message string `json:"message"`
}

// Error fills the error interface
func (e Error) Error() string {
	return "gql: " + e.Message
}

// Errors is a collection of GraphQL errors
type Errors []Error

// Error fills the error interface
func (e Errors) Error() string {
	errs := make([]string, len(e))
	for idx, err := range e {
		errs[idx] = err.Error()
	}
	return strings.Join(errs, "\n")
}

// Response is the response to a GraphQL Request
type Response struct {
	Data   interface{} `json:"data"`
	Errors Errors      `json:"errors"`
}
